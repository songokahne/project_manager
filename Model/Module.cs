﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Module
    {
        #region Globals

        #endregion

        #region Events

        #endregion

        #region Properties
        public string Icon { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        #endregion

        #region Constructor
        public Module(string icon, string name, string description)
        {
            Icon = icon;
            Name = name;
            Description = description;
        }
        #endregion

        #region Methods

        #endregion
    }
}
