﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Employee
    {
        #region Globals

        #endregion

        #region Events

        #endregion

        #region Properties
        public int ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public DateTime Birthday { get; set; }
        public char Gender { get; set; }
        public string Portrait { get; set; }
        public string IBAN { get; set; }
        public string BIC { get; set; }
        #endregion

        #region Constructor
        public Employee(int id, string lastName, string firstname, string street, string zipcode, string city, DateTime birthday, char gender, string portrait, string iban, string bic)
        {
            ID = id;
            LastName = lastName;
            FirstName = firstname;
            Street = street;
            ZipCode = zipcode;
            City = city;
            Birthday = birthday;
            Gender = gender;
            Portrait = portrait;
            IBAN = iban;
            BIC = bic;
        }
        #endregion

        #region Methods

        #endregion
    }
}
