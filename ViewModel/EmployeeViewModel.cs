﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Common;
using Model;

namespace ViewModel
{
    public class EmployeeViewModel : ViewModelBase
    {
        #region Globals
        private Employee _employee;
        #endregion

        #region Events

        #endregion

        #region Properties
        public int ID
        {
            get { return _employee.ID; }
            set
            {
                _employee.ID = value;
                OnPropertyChanged("ID");
            }
        }
        public string LastName
        {
            get { return _employee.LastName; }
            set
            {
                _employee.LastName = value;
                OnPropertyChanged("LastName");
            }
        }
        public string FirstName
        {
            get { return _employee.FirstName; }
            set
            {
                _employee.FirstName = value;
                OnPropertyChanged("FirstName");
            }
        }
        public string Street
        {
            get { return _employee.Street; }
            set
            {
                _employee.Street = value;
                OnPropertyChanged("Street");
            }
        }
        public string ZipCode
        {
            get { return _employee.ZipCode; }
            set
            {
                _employee.ZipCode = value;
                OnPropertyChanged("ZipCode");
            }
        }
        public string City
        {
            get { return _employee.City; }
            set
            {
                _employee.City = value;
                OnPropertyChanged("City");
            }
        }
        public DateTime Birthday
        {
            get { return _employee.Birthday; }
            set
            {
                _employee.Birthday = value;
                OnPropertyChanged("Birthday");
            }
        }
        public char Gender
        {
            get { return _employee.Gender; }
            set
            {
                _employee.Gender = value;
                OnPropertyChanged("Gender");
            }
        }
        public string Portrait
        {
            get { return _employee.Portrait; }
            set
            {
                _employee.Portrait = value;
                OnPropertyChanged("Portrait");
            }
        }
        public string IBAN
        {
            get { return _employee.IBAN; }
            set
            {
                _employee.IBAN = value;
                OnPropertyChanged("IBAN");
            }
        }
        public string BIC
        {
            get { return _employee.BIC; }
            set
            {
                _employee.BIC = value;
                OnPropertyChanged("BIC");
            }
        }
        #endregion

        #region Constructor
        public EmployeeViewModel(Employee employee)
        {
            _employee = employee;
        }
        #endregion

        #region Methods

        #endregion
    }
}
