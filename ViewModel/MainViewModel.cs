﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ViewModel.Common;

namespace ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Globals

        private ContentControl _pageHoster;

        // Pages
        private Page _main;

        // ViewModels
        

        #endregion

        #region Properties

        #endregion

        #region Constructor

        public MainViewModel(ContentControl pageHoster)
        {
            _pageHoster = pageHoster;
            InitializePages();
            ShowMain();
        }

        #endregion

        #region Commands

        #endregion

        #region Methods
        private void InitializePages()
        {
            // Main
            _main = new Page();
        }

        private void ShowMain()
        {
            _pageHoster.DataContext = _main;
        }

        #endregion
    }
}
