﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Common;
using ViewModel.Extensions;
using Model;

namespace ViewModel.Lists
{
    class EmployeeListViewModel : ViewModelBase
    {
        #region Globals
        private ObservableCollection<EmployeeViewModel> _employees;
        #endregion

        #region Events
        public delegate void OnShowEmployeeDetailsEventHandler(object sender, EmployeeViewModel employeeViewModel);
        public event OnShowEmployeeDetailsEventHandler OnShowEmployeeDetails;
        #endregion

        #region Properties
        public ObservableCollection<EmployeeViewModel> Employees
        {
            get { return _employees; }
            set
            {
                _employees = value;
                OnPropertyChanged("Employees");
            }
        }
        public EmployeeViewModel SelectedEmployee { get; set; }

        #endregion

        #region Constructor
        public EmployeeListViewModel()
        {

        }
        #endregion

        #region Methods
        private void Initialize()
        {
            Employees = new ObservableCollection<EmployeeViewModel>()
            {
                new EmployeeViewModel(new Employee(1, "Barkmann", "Dominik", "Platanenweg 18", "22846", "Norderstedt", DateTime.Parse("07.05.1993"), 'M', "", "", ""))
            }.ToObservableCollection();
        }
        #endregion
    }
}
