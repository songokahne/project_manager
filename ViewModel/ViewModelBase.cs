﻿using System;
using System.ComponentModel;


namespace ViewModel
{
    public class ViewModelBase
    {
        #region Globals

        #endregion

        #region Events

        [field: NonSerializedAttribute()]
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        #endregion

        #region Constructor

        #endregion

        #region Methods

        protected internal void OnPropertyChanged(string propertyname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }

        #endregion
    }
}
